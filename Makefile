KMOD=uniac-mpu6050.ko
KDEV=mpu6050-overlay
DIR=$(PWD)
BUILD_KERNEL=/lib/modules/`uname -r`/build
OBJS=src/sysfs_face.o src/mpu6050/mpu6050_i2c.o src/uniac-mpu6050.o

obj-m += uniac-mpu6050.o
uniac-mpu6050-objs:=$(OBJS)

INSMOD_ARGS=

all:
	make -C ${BUILD_KERNEL} M=${DIR} modules
	mkdir -p ${DIR}/bin
	mv ${DIR}/${KMOD} ${DIR}/bin/${KMOD}
	dtc -@ -I dts -O dtb -o ${DIR}/bin/${KDEV}.dtbo ${DIR}/src/${KDEV}.dts

clean:
	make -C ${BUILD_KERNEL} M=${DIR} clean
	rm -f ${DIR}/bin/${KDEV}.dtbo

install: all
	dmesg -c
	insmod ${DIR}/bin/${KMOD} ${INSMOD_ARGS}