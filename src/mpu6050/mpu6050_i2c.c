#include "mpu6050_i2c.h"

#define ABS(a) (a < 0 ? ~a + 1 : a)

static const struct i2c_client *mpu6050_i2c_client;
static int32_t precision;
static int32_t accel_div, gyro_div;
static const uint16_t accel_sel2range[] = {
	16384,
	8192,
	4096,
	2048
};

static const uint16_t gyro_sel2range[4] = {
	131,
	66,
	33,
	16
};

static int8_t mpu6050_read_byte(uint8_t reg) {
	return i2c_smbus_read_byte_data(mpu6050_i2c_client, reg);
}

static int16_t mpu6050_read_word(uint8_t reg) {
	uint16_t w = i2c_smbus_read_word_data(mpu6050_i2c_client, reg);
	return ((w << 8) | (w >> 8));
}

static int mpu6050_write_byte(uint8_t reg, uint8_t data) {
	return i2c_smbus_write_byte_data(mpu6050_i2c_client, reg, data);
}

void mpu6050_default_config(const struct i2c_client *client, int32_t prec) {
	mpu6050_i2c_client = client;
	precision = prec;
	if (precision < 100) {
		precision = 100;
	}
	// RESET
	mpu6050_write_byte(SIGNAL_PATH_RESET_REG, 0x7);
	mpu6050_write_byte(SIGNAL_PATH_RESET_REG, 0x0);
	mpu6050_write_byte(PWR_MGMT_1_REG, 0x80);
	printk("PWR: %x ACCEL: %x GYRO: %x\n", mpu6050_read_byte(PWR_MGMT_1_REG), 
	mpu6050_read_byte(ACCEL_CONFIG_REG), mpu6050_read_byte(GYRO_CONFIG_REG));
	
	mpu6050_write_byte(PWR_MGMT_1_REG, 0x1);
	mpu6050_write_byte(SMPRT_DIV_REG, 0x0);
	mpu6050_write_byte(CONFIG_REG, 0x1);
	mpu6050_write_byte(GYRO_CONFIG_REG, 0x8);
	mpu6050_write_byte(ACCEL_CONFIG_REG, 0x8);
	printk("PWR: %x ACCEL: %x GYRO: %x\n", mpu6050_read_byte(PWR_MGMT_1_REG), 
	mpu6050_read_byte(ACCEL_CONFIG_REG), mpu6050_read_byte(GYRO_CONFIG_REG));
	accel_div = accel_sel2range[(mpu6050_read_byte(ACCEL_CONFIG_REG) & ACCEL_SEL_MASK) >> 3];
	gyro_div = gyro_sel2range[(mpu6050_read_byte(GYRO_CONFIG_REG) & GYRO_SEL_MASK) >> 3];
}

int32_t mpu6050_precision(void) {
	return precision;
}

int mpu6050_check_device(const struct i2c_client *client) {
	uint8_t who_am_i = (uint8_t)i2c_smbus_read_byte_data(client, WHO_AM_I_REG);
	return (who_am_i == 0x68);
}

int32_t mpu6050_temp(void) {
	int16_t t = mpu6050_read_word(TEMP_OUT_REG);
	return (t * precision) / 340 + 3653 * (precision / 100);
}

#define mpu6050_reg_reader(name, reg, div) 		\
int32_t name(void) {							\
	int16_t val = mpu6050_read_word(reg);		\
	int16_t abs_val = ABS(val);					\
	int32_t r = (abs_val * precision) / div;	\
	if (val < 0) {								\
		r = -r;									\
	}											\
	return r;									\
}

mpu6050_reg_reader(mpu6050_accel_x, ACCEL_XOUT_REG, accel_div)
mpu6050_reg_reader(mpu6050_accel_y, ACCEL_YOUT_REG, accel_div)
mpu6050_reg_reader(mpu6050_accel_z, ACCEL_ZOUT_REG, accel_div)

mpu6050_reg_reader(mpu6050_gyro_x, GYRO_XOUT_REG, gyro_div)
mpu6050_reg_reader(mpu6050_gyro_y, GYRO_YOUT_REG, gyro_div)
mpu6050_reg_reader(mpu6050_gyro_z, GYRO_ZOUT_REG, gyro_div)