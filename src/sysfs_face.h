#ifndef SYSFS_FACE_H_
#define SYSFS_FACE_H_

#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/version.h>
#include <linux/parport.h>
#include <linux/pci.h>
#include <linux/string.h>

#include "mpu6050/mpu6050_i2c.h"

int sysfs_face_init(void);
void sysfs_face_exit(void);

#endif // SYSFS_FACE_H_