#include "sysfs_face.h"


static uint32_t precision;

#define MPU_READER(name, func, ...) 														\
static ssize_t name##_show(struct class *class, struct class_attribute *attr, char *buf) { 	\
	int32_t r = func(__VA_ARGS__);															\
	int32_t abs_r = (r < 0 ? ~(int32_t)r + 1 : r); 											\
	int16_t h = abs_r / precision;															\
	int16_t l = abs_r % precision;															\
	if (r < 0) {																			\
		sprintf(buf, "-%d.%03d\n", h, l);													\
	} else {																				\
		sprintf(buf, "%d.%03d\n", r / precision, r % precision);							\
	}																						\
	return strlen(buf);																		\
}																							\
CLASS_ATTR_RO(name)

MPU_READER(temp, mpu6050_temp);
MPU_READER(accel_x, mpu6050_accel_x);
MPU_READER(accel_y, mpu6050_accel_y);
MPU_READER(accel_z, mpu6050_accel_z);
MPU_READER(gyro_x, mpu6050_gyro_x);
MPU_READER(gyro_y, mpu6050_gyro_y);
MPU_READER(gyro_z, mpu6050_gyro_z);

static struct class *mpu6050_class;

int sysfs_face_init(void) {
	precision = mpu6050_precision();
	int res = 0;
	mpu6050_class = class_create(THIS_MODULE, "mpu6050");
	if (IS_ERR(mpu6050_class)) {
		res = -1;
		goto err;
	}
	res = class_create_file(mpu6050_class, &class_attr_temp);
	res += class_create_file(mpu6050_class, &class_attr_accel_x);
	res += class_create_file(mpu6050_class, &class_attr_accel_y);
	res += class_create_file(mpu6050_class, &class_attr_accel_z);
	res += class_create_file(mpu6050_class, &class_attr_gyro_x);
	res += class_create_file(mpu6050_class, &class_attr_gyro_y);
	res += class_create_file(mpu6050_class, &class_attr_gyro_z);
err:
	return res;
}

void sysfs_face_exit(void) {
	class_remove_file(mpu6050_class, &class_attr_temp);
	class_remove_file(mpu6050_class, &class_attr_accel_x);
	class_remove_file(mpu6050_class, &class_attr_accel_y);
	class_remove_file(mpu6050_class, &class_attr_accel_z);
	class_remove_file(mpu6050_class, &class_attr_gyro_x);
	class_remove_file(mpu6050_class, &class_attr_gyro_y);
	class_remove_file(mpu6050_class, &class_attr_gyro_z);
	class_destroy(mpu6050_class);
}
